import pathlib


def shadow_gitignore_align(missing_from_gitignore_list: list, gitignore_path: pathlib.PosixPath) -> None:
    with open(gitignore_path.as_posix(), "a") as gitignore:
        gitignore.writelines([x+"\n" for x in missing_from_gitignore_list])


def shadow_gitignore_diffs(gitignore_path: pathlib.PosixPath, shadow_file_path: pathlib.PosixPath) -> list:
    with open(gitignore_path.as_posix(), "r") as gitignore, open(shadow_file_path.as_posix(), "r") as shadow:
        return [x.strip() for x in shadow.readlines() if x not in gitignore.readlines()]
