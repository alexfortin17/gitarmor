"""
Init_utils
============
This module encompasses functions that are used my the "gitarmor init" command
"""

import git
import pathlib
import os
from gitarmor_exceptions import gitarmor_exception
from utils.armor_utils import shadow_gitignore_align, shadow_gitignore_diffs
from shutil import rmtree
from gnupg import GPG


"""Init_utils"""

def create_dot_gnupg_dir(repo_path: pathlib.PosixPath):
    """

    :param repo_path: root of the repo
    :return:
    """

    # verify it it already exists
    gnupg_path = os.path.join(repo_path.as_posix(), ".gnupg")
    if os.path.isdir(gnupg_path):
        raise gitarmor_exception.GitArmorError("a .gnupg directory is already prsent in this repo")
    GPG(gnupghome=os.path.join(repo_path.as_posix(), ".gnupg"))

    # TODO ask for pubkey and add pubkey to initialize keyring
    return


def verify_inputs(repo_path: pathlib.PosixPath) -> None:
    """
    Verify the inputs of the init command.
    Raises GitArmorError if anomaly is found

    :param repo_path: Path to the root of the repo
    :rtype: None
    :raises: GitArmorError
    """
    # verify types
    if type(repo_path) is not pathlib.PosixPath:
        raise gitarmor_exception.GitArmorTypeError \
            ("Expected pathlib.PosixPath type, got {} instead".format(type(repo_path)))

    # verify if repo exists
    if not repo_path.exists():
        raise gitarmor_exception.GitArmorError \
            ("The path provided to initialize the repo is invalid")

    if not repo_path.is_dir():
        raise gitarmor_exception.GitArmorError \
            ("The path provided does not refer to a directory")


def verify_or_create_git_repo(dir_path: pathlib.PosixPath) -> pathlib.PosixPath:
    """
    Verify repo_path is within a git repo and if not, initialises a new git repo with repo_path as root
    Also makes sure the .gitignore file exists

    :param dir_path: Path to directory where the search will start
    :return: root of the git repo
    :rtype: pathlib.PosixPath
    """
    # if not already a git repo
    if not is_git_repo(dir_path):
        # initialize git repo
        print("It appears no Git repo is present at {}".format(dir_path.as_posix()))
        print("Creating ....")
        git.Repo.init(path=dir_path.as_posix())
        # create .gitignore
        open(os.path.join(dir_path.as_posix(), ".gitignore"), "a").close()
        return dir_path
    else:
        repo_root = get_git_root(dir_path)
        if ".gitignore" not in os.listdir(repo_root.as_posix()):
            open(os.path.join(dir_path.as_posix(), ".gitignore"), "a").close()
        return repo_root


def create_dot_dir(dir_path: pathlib.PosixPath, force_create=False) -> pathlib.PosixPath:
    """
    Create the .armor.d directory

    :param dir_path: directory where to create the .armor.d directory
    :param force_create: determine if files will be overwritten if .armor.d already exists
    :return: full path of the .armor.d directory
    :rtype: pathlib.PosixPath
    """
    # check if its already of gitArmor repo
    if dot_dir_exists(dir_path):
        if not force_create:
            raise gitarmor_exception.GitArmorDotDirExistsError \
                ("It appears that this repo is already a gitarmor repo")
        else:
            rmtree(os.path.join(dir_path.as_posix(), ".armor.d"), ignore_errors=True)

    # Makes sure the armor.d dir is in the same dir as a .git repo
    if ".git" not in os.listdir(dir_path.as_posix()):
        raise gitarmor_exception.GitArmorError(".armor.d directory must be in same directory as .git")

    dot_dir_path_str = os.path.join(dir_path.as_posix(), ".armor.d")
    dot_dir_path = pathlib.Path(dot_dir_path_str)
    pathlib.Path.mkdir(dot_dir_path, mode=0o700, exist_ok=True)

    return pathlib.PosixPath(dot_dir_path)


def create_initial_shadow_file(dot_dir_path: pathlib.PosixPath, git_repo_root: pathlib.PosixPath) -> None:
    """
    Create initial shadow file with only the .shadow string

    :param git_repo_root: root of the git repo
    :param dot_dir_path: Path to .armor.d directory
    """
    # make sure its a file
    if not dot_dir_path.is_dir():
        raise gitarmor_exception.GitArmorError("the dotDirPath argument must be a directory")
    # make sur the parent is the DotDirectory
    if not dot_dir_path.name == ".armor.d":
        raise gitarmor_exception.GitArmorError("the dotDirPath argument must be called .armor")

    with open(os.path.join(dot_dir_path.as_posix(), "shadow"), "w") as f:
        # writes default .shadow extension in shadow file
        f.write(".shadow")
    # Make sure .gitignore is in line
    diff_list = \
        shadow_gitignore_diffs(gitignore_path=pathlib.PosixPath(os.path.join(git_repo_root.as_posix(), ".gitignore")),
                               shadow_file_path=pathlib.PosixPath(os.path.join(dot_dir_path.as_posix(), "shadow")))

    shadow_gitignore_align(missing_from_gitignore_list=diff_list,
                           gitignore_path=pathlib.PosixPath(os.path.join(git_repo_root.as_posix(), ".gitignore")))

def is_git_repo(path: pathlib.PosixPath) -> bool:
    """Recursively move up directory to find a .git file"""
    if path.is_file():
        current_dir = os.path.dirname(path.as_posix())
    else:
        current_dir = path.as_posix()

    # recursion stop condition is finding the root of the system as current_dir
    while current_dir != os.sep:
        dir_content = os.listdir(current_dir)
        for item in dir_content:
            if item == '.git':
                return True
        current_dir = os.path.dirname(current_dir)

    return False


def get_git_root(path: pathlib.PosixPath) -> pathlib.PosixPath:
    """Recursively move up directory to find a .git file"""
    if path.is_file():
        current_dir = os.path.dirname(path.as_posix())
    else:
        current_dir = path.as_posix()

    # recursion stop condition is finding the root of the system as current_dir
    while current_dir != os.sep:
        dir_content = os.listdir(current_dir)
        for item in dir_content:
            if item == '.git':
                return pathlib.PosixPath(current_dir)
        current_dir = os.path.dirname(current_dir)

    raise gitarmor_exception.GitArmorError("No git repo was found")


def dot_dir_exists(dir_path: pathlib.PosixPath) -> bool:
    # check if its already of gitArmor repo
    dot_dir_path = pathlib.PosixPath(os.path.join(dir_path.as_posix(), ".armor.d"))
    if dot_dir_path.exists():
        return True
    else:
        return False


def verify_home_dotgnupg():
    pass