import gnupg
from pathlib import PosixPath
from gitarmor_exceptions.gitarmor_exception import GitArmorError

def transfer_pub_key(srckeybox_path: PosixPath, destkeybox_path: PosixPath, pub_key_id: str):
    """

    :param srckeybox_path: Path of source .gnupg directory
    :param destkeybox_path: Path of destination .gnupg directory
    :param pub_key_id: gnupg key id of public key to be transferred from source to destination
    :return:
    """

    src_keybox = gnupg.GPG(gnupghome=srckeybox_path.as_posix())
    dest_keybox = gnupg.GPG(gnupghome=destkeybox_path.as_posix())

    pub_key_ascii = src_keybox.export_keys(pub_key_id)

    res = dest_keybox.import_keys(pub_key_ascii)
    if res.count != 1:
        raise GitArmorError("Public key {} failed to import".format(pub_key_ascii))


def choose_pubkey(srckeybox_path: PosixPath, cherry_pick=False) -> str:
    """

    :param srckeybox_path: Path of .gnupg directory
    :param cherry_pick: let user choose specific key from keybox or not
    :return: Ascci output of public key as a string
    """
    keybox = gnupg.GPG(gnupghome=srckeybox_path.as_posix())
    if len(keybox.list_keys()) == 0:
        gen_new_key_pair(keybox)

    keylist = keybox.list_keys()

    for no, key in enumerate(keylist):
        print("KeyNo: {}: Uids:{} Length:{} Fingerprint:{} KeyID:{}"
              .format(no, key['uids'], key['length'], key['fingerprint'], key['keyid']))

    if len(keylist) > 1 and cherry_pick is True:
        while True:
            try:
                pickkey = int(input('Choose the KeyNo you want to use (0,1,2,3,...) : '))
                break
            except:
                print("That's not a valid option!")
    else:
        # takes first key as default
        pickkey = 0

    return keybox.export_keys(keylist[pickkey]['keyid'])

def gen_new_key_pair(keybox: gnupg.GPG):
    pass