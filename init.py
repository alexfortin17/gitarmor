import pathlib
from gitarmor_exceptions import gitarmor_exception
from utils.init_utils import verify_inputs, verify_or_create_git_repo, create_dot_dir,\
    create_initial_shadow_file, create_dot_gnupg_dir, verify_home_dotgnupg
import sys


def initialize(path: pathlib.PosixPath):
    try:

        verify_inputs(path)

        git_repo_root = verify_or_create_git_repo(path)

        dot_dir_path = create_dot_dir(git_repo_root, force_create=True)

        create_initial_shadow_file(dot_dir_path, git_repo_root)

        create_dot_gnupg_dir(path)

        verify_home_dotgnupg()

    except gitarmor_exception.GitArmorTypeError as shadow_err:
        print("**gitarmor is not having this duck typing bullshit**")
        print(shadow_err)
        sys.exit(-1)

    except gitarmor_exception.GitArmorDotDirExistsError as shadow_err:
        print(shadow_err)
        print("You can use the force_create flag to force the reinitialization"
              " of the .armor.d directory and all its components")
        sys.exit(-1)

    except gitarmor_exception.GitArmorError as shadow_err:
        print("### gitarmor error ###*")
        print(shadow_err)
        sys.exit(-1)

    except Exception as ex:
        print(ex)
        sys.exit(-1)
