class GitArmorError(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class GitArmorTypeError(GitArmorError):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class GitArmorDotDirExistsError(GitArmorError):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)
