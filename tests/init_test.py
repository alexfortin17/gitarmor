import unittest
import git
import pathlib
from utils import init_utils
from gitarmor_exceptions import gitarmor_exception
import tempfile
import os


class TestVerifyInputs(unittest.TestCase):
    """If wrong type is provided, GitArmorTypeError is raised"""
    def testWrongType(self):
        temp = "/tmp"
        with self.assertRaises(gitarmor_exception.GitArmorTypeError):
            init_utils.verify_inputs(temp)

    def testInvalidPath(self):
        """If a non existent path is provided as repo path, GitArmorError is raised"""
        temp = "/tmp2"
        with self.assertRaises(gitarmor_exception.GitArmorError):
            init_utils.verify_inputs(pathlib.PosixPath(temp))

    def testFilePath(self):
        """Verify that if a file is passed as path instead of a directory, GitArmorError is raised"""
        with tempfile.NamedTemporaryFile() as tempofile:
            with self.assertRaises(gitarmor_exception.GitArmorError):
                init_utils.verify_inputs(pathlib.PosixPath(tempofile.name))


class TestVerifyOrCreateGitRepo(unittest.TestCase):
    """As a user, I want to be able to use git armor for both creating a brand new git repo or to augment an existing
    git repo to a gitarmor repo
    """

    def testNonGitRepoCreateGitRepo(self):
        """Verify if a dir that is not a git repo becomes a git repo after call
            Also verify if .gitignore is created"""
        with tempfile.TemporaryDirectory() as tempdir:
            init_utils.verify_or_create_git_repo(pathlib.PosixPath(tempdir))
            self.assertTrue(init_utils.is_git_repo(pathlib.PosixPath(tempdir)))
            self.assertTrue(".gitignore" in os.listdir(tempdir))

    def testAlreadyGitRepoCreateGitRepo(self):
        """Verify if a dir that is a git repo is still a git repo
            Also verify if .gitignore is created"""
        with tempfile.TemporaryDirectory() as tempdir:
            git.Repo.init(path=tempdir)
            init_utils.verify_or_create_git_repo(pathlib.PosixPath(tempdir))
            self.assertTrue(init_utils.is_git_repo(pathlib.PosixPath(tempdir)))
            self.assertTrue(".gitignore" in os.listdir(tempdir))

    def testAlreadyGitRepoWithGitIgnoreCreateGitRepo(self):
        """Verify if a dir that is a git repo is still a git repo
            Also verify that if .gitignore is already there, we don't touch it"""
        with tempfile.TemporaryDirectory() as tempdir:
            git.Repo.init(path=tempdir)
            open(os.path.join(tempdir, ".gitignore"), "a").close()
            mtime = os.stat(os.path.join(tempdir, ".gitignore")).st_mtime
            init_utils.verify_or_create_git_repo(pathlib.PosixPath(tempdir))
            self.assertTrue(init_utils.is_git_repo(pathlib.PosixPath(tempdir)))
            self.assertTrue(os.stat(os.path.join(tempdir, ".gitignore")).st_mtime == mtime)


class TestCreateDotDir(unittest.TestCase):

    def testIsArmorRepo(self):
        with tempfile.TemporaryDirectory() as tempdir:
            init_utils.verify_or_create_git_repo(pathlib.PosixPath(tempdir))
            os.mkdir(os.path.join(tempdir, ".armor.d"))
            with self.assertRaises(gitarmor_exception.GitArmorDotDirExistsError):
                init_utils.create_dot_dir(pathlib.PosixPath(tempdir))

    def testArmorRepoIsPresent(self):
        with tempfile.TemporaryDirectory() as tempdir:
            git.Repo.init(tempdir)
            init_utils.create_dot_dir(pathlib.PosixPath(tempdir))
            self.assertTrue(os.path.isdir(os.path.join(tempdir, ".armor.d")))

    def testDotGitNotPresent(self):
        with tempfile.TemporaryDirectory() as tempdir:
            with self.assertRaises(gitarmor_exception.GitArmorError):
                init_utils.create_dot_dir(pathlib.PosixPath(tempdir))

    def testDotDirAlreadyPresent(self):
        with tempfile.TemporaryDirectory() as tempdir:
            git.Repo.init(tempdir)
            os.mkdir(os.path.join(tempdir, ".armor.d"))
            open(os.path.join(os.path.join(tempdir, ".armor.d"), "vomi"), "a").close()
            dot_dir_path = init_utils.create_dot_dir(pathlib.PosixPath(tempdir), force_create=True)
            self.assertFalse(os.path.isfile(os.path.join(dot_dir_path.as_posix(), "vomi")))


class TestCreateShadowFile(unittest.TestCase):

    def testNotUnderDotArmor(self):
        with tempfile.TemporaryDirectory() as tempdir:
            with self.assertRaises(gitarmor_exception.GitArmorError):
                init_utils.create_initial_shadow_file(pathlib.PosixPath(tempdir), pathlib.PosixPath(tempdir))

    def testNotEvenADir(self):
        with tempfile.TemporaryDirectory() as tempdir:
            os.mkdir(os.path.join(tempdir, ".armor.d"))
            open(os.path.join(os.path.join(tempdir, ".armor.d"), "vomi"), "a").close()
            with self.assertRaises(gitarmor_exception.GitArmorError):
                init_utils.create_initial_shadow_file(pathlib.PosixPath(os.path.join(os.path.join(tempdir, ".armor.d"), "vomi")), pathlib.PosixPath(tempdir))

    def testUnderDotArmor(self):
        with tempfile.TemporaryDirectory() as tempdir:
            init_utils.verify_or_create_git_repo(pathlib.PosixPath(tempdir))
            dot_dir_path = init_utils.create_dot_dir(pathlib.PosixPath(tempdir))
            init_utils.create_initial_shadow_file(dot_dir_path, pathlib.PosixPath(tempdir))
            self.assertTrue("shadow" in os.listdir(dot_dir_path.as_posix()))
            line1 = ""
            with open(os.path.join(dot_dir_path.as_posix(), "shadow"), "r") as f:
                line1 = f.readline()
            self.assertTrue(line1 == ".shadow")

    def testGitIgnoreAlignment(self):
        with tempfile.TemporaryDirectory() as tempdir:
            init_utils.verify_or_create_git_repo(pathlib.PosixPath(tempdir))
            dot_dir_path = init_utils.create_dot_dir(pathlib.PosixPath(tempdir))
            init_utils.create_initial_shadow_file(dot_dir_path, pathlib.PosixPath(tempdir))
            line1 = ""
            with open(os.path.join(tempdir, ".gitignore"), "r") as f:
                line1 = f.readline()
            self.assertTrue(line1 == ".shadow\n")


class TestIsGitRepo(unittest.TestCase):

    def testNonRepo(self):
        temp = "/tmp"
        with self.assertRaises(git.InvalidGitRepositoryError):
            git.Repo(temp)

    def testNonRepo2(self):
        temp = "/tmp"
        init_utils.is_git_repo(pathlib.PosixPath(temp))
        self.assertFalse(init_utils.is_git_repo(pathlib.PosixPath(temp)))

    def testIsRepo(self):
        """Simple check of the initialization of a new git repo"""
        with tempfile.TemporaryDirectory() as tempdir:
            git.Repo.init(tempdir)
            self.assertTrue(init_utils.is_git_repo(pathlib.PosixPath(tempdir)))

    def testIsRepoRecursive(self):
        """Checks if the is_git_repo funtion returns True when path that it gets is further down the tree
        (Not in the same directory as .git)"""
        with tempfile.TemporaryDirectory() as tempdir:
            git.Repo.init(tempdir)
            os.mkdir(os.path.join(tempdir, "test"))
            self.assertTrue(init_utils.is_git_repo(pathlib.PosixPath(os.path.join(tempdir, "test"))))

    def testIsRepoRecursiveFromFile(self):
        """Checks if the is_git_repo funtion returns True when path that it gets is further down the tree
        (Not in the same directory as .git) and when a file is given as path"""
        with tempfile.TemporaryDirectory() as tempdir:
            git.Repo.init(tempdir)
            os.mkdir(os.path.join(tempdir, "test"))
            open(os.path.join(os.path.join(tempdir, "test"), "vomi"), "a").close()
            self.assertTrue(init_utils.is_git_repo(pathlib.PosixPath(os.path.join(os.path.join(tempdir, "test"), "vomi"))))

class TestGetGitRoot(unittest.TestCase):

    def testIsRepoRecursive(self):
        """Checks if the is_git_repo funtion returns True when path that it gets is further down the tree
        (Not in the same directory as .git)"""
        with tempfile.TemporaryDirectory() as tempdir:
            git.Repo.init(tempdir)
            os.mkdir(os.path.join(tempdir, "test"))
            self.assertTrue(init_utils.get_git_root(pathlib.PosixPath(os.path.join(tempdir, "test"))).as_posix() == tempdir)

    def testIsRepoRecursiveFromFile(self):
        """Checks if the is_git_repo funtion returns True when path that it gets is further down the tree
        (Not in the same directory as .git) and when a file is given as path"""
        with tempfile.TemporaryDirectory() as tempdir:
            git.Repo.init(tempdir)
            os.mkdir(os.path.join(tempdir, "test"))
            open(os.path.join(os.path.join(tempdir, "test"), "vomi"), "a").close()
            self.assertTrue(init_utils.get_git_root(pathlib.PosixPath(os.path.join(os.path.join(tempdir, "test"), "vomi"))).as_posix() == tempdir)

    def testNotGitRepo(self):
        """Simple check of the initialization of a new git repo"""
        with tempfile.TemporaryDirectory() as tempdir:
            with self.assertRaises(gitarmor_exception.GitArmorError):
                init_utils.get_git_root(pathlib.PosixPath(tempdir))

class TestdotGnupgDir(unittest.TestCase):
    def testCreateGnupg(self):
        with tempfile.TemporaryDirectory() as tempdir:
            init_utils.create_dot_gnupg_dir(pathlib.PosixPath(tempdir))
            self.assertTrue(os.path.isdir(os.path.join(tempdir, ".gnupg")))

    def testGnupgAlreadyThere(self):
        with tempfile.TemporaryDirectory() as tempdir:
            os.mkdir(os.path.join(tempdir, ".gnupg"))
            with self.assertRaises(gitarmor_exception.GitArmorError):
                init_utils.create_dot_gnupg_dir(pathlib.PosixPath(tempdir))

if __name__ == '__main__':
    unittest.main()
