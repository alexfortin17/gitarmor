from utils import armor_utils
import unittest
import git
from pathlib import PosixPath
from utils import init_utils
from gitarmor_exceptions import gitarmor_exception
import tempfile
import os


class TestGitIgnoreDiffs(unittest.TestCase):
    def testAlreadyAllInGitIgn(self):
        with tempfile.NamedTemporaryFile() as tempIgnfile, tempfile.NamedTemporaryFile() as tempShadowFile:
            ign_content = ["*.txt", "*.o", "*.a"]
            shadow_content = ["*.txt"]
            with open(tempIgnfile.name, "a") as f:
                f.writelines("%s\n" % l for l in ign_content)
            with open(tempShadowFile.name, "a") as f:
                f.writelines("%s\n" % l for l in shadow_content)
            diffs = armor_utils.shadow_gitignore_diffs(PosixPath(tempIgnfile.name), PosixPath(tempShadowFile.name))
            print(diffs)
            self.assertTrue(len(diffs) == 0)

    def test2ItemsMissing(self):
        with tempfile.NamedTemporaryFile() as tempIgnfile, tempfile.NamedTemporaryFile() as tempShadowFile:
            shadow_content = ["*.txt", "*.o", "*.a"]
            ign_content = ["*.txt"]
            with open(tempIgnfile.name, "a") as f:
                f.writelines("%s\n" % l for l in ign_content)
            with open(tempShadowFile.name, "a") as f:
                f.writelines("%s\n" % l for l in shadow_content)
            diffs = armor_utils.shadow_gitignore_diffs(PosixPath(tempIgnfile.name), PosixPath(tempShadowFile.name))
            print(diffs)
            self.assertTrue(len(diffs) == 2)


class TestGitIgnoreAlign(unittest.TestCase):

    def test2ItemsMissing(self):
        with tempfile.NamedTemporaryFile() as tempIgnfile, tempfile.NamedTemporaryFile() as tempShadowFile:
            shadow_content = ["*.txt", "*.o", "*.a"]
            ign_content = ["*.txt"]
            with open(tempIgnfile.name, "a") as f:
                f.writelines("%s\n" % l for l in ign_content)
            with open(tempShadowFile.name, "a") as f:
                f.writelines("%s\n" % l for l in shadow_content)
            diffs = armor_utils.shadow_gitignore_diffs(PosixPath(tempIgnfile.name), PosixPath(tempShadowFile.name))
            print(diffs)
            self.assertTrue(len(diffs) == 2)
            armor_utils.shadow_gitignore_align(diffs, PosixPath(tempIgnfile.name))
            with open(tempIgnfile.name, "r") as f:
                gitigncontent = f.readlines()
                for x in shadow_content:
                    self.assertTrue(x.strip() in [y.strip() for y in gitigncontent])
