from utils import gnupg_utils
import tempfile
import unittest
import os
import pathlib
import gnupg

class TestdotGnupgDir(unittest.TestCase):
    def testTransferPubKey(self):
        """Transfer only key from src keybox to empty dest keybox"""
        with tempfile.TemporaryDirectory() as tempdir:
            os.mkdir(os.path.join(tempdir,"test"))
            os.mkdir(os.path.join(tempdir, ".gnupg"))
            os.mkdir(os.path.join(tempdir, "test",".gnupg"))
            srckeybox_path = pathlib.PosixPath(os.path.join(tempdir, ".gnupg"))
            destkeybox_path = pathlib.PosixPath(os.path.join(tempdir,"test", ".gnupg"))
            srckeybox = gnupg.GPG(gnupghome=srckeybox_path.as_posix())
            destkeybox = gnupg.GPG(gnupghome=destkeybox_path.as_posix())

            input_data = srckeybox.gen_key_input(key_type="RSA", key_length=1024, name_email="test@test", passphrase="vomi")
            key = srckeybox.gen_key(input_data)
            #input_data2 = destkeybox.gen_key_input(key_type="RSA", key_length=2048, name_email="test2@test2")
            #key2 = destkeybox.gen_key(input_data2)
            self.assertTrue(len(srckeybox.list_keys()) == 1)
            keyid = srckeybox.list_keys()[0]["keyid"]
            fingerprint = srckeybox.list_keys()[0]["fingerprint"]
            self.assertTrue(len(destkeybox.list_keys()) == 0)

            gnupg_utils.transfer_pub_key(srckeybox_path, destkeybox_path, keyid)
            self.assertTrue(len(destkeybox.list_keys()) == 1)
            keyid2 = destkeybox.list_keys()[0]["keyid"]
            fingerprint2 = destkeybox.list_keys()[0]["fingerprint"]

            self.assertTrue(keyid == keyid2)
            self.assertTrue(fingerprint == fingerprint2)

    def testChooseDefaultPubkey(self):
        """One pub key in keybox and no cherry picking """
        with tempfile.TemporaryDirectory() as tempdir:
            os.mkdir(os.path.join(tempdir,"test"))
            os.mkdir(os.path.join(tempdir, ".gnupg"))
            os.mkdir(os.path.join(tempdir, "test",".gnupg"))
            srckeybox_path = pathlib.PosixPath(os.path.join(tempdir, ".gnupg"))
            destkeybox_path = pathlib.PosixPath(os.path.join(tempdir,"test", ".gnupg"))
            srckeybox = gnupg.GPG(gnupghome=srckeybox_path.as_posix())
            destkeybox = gnupg.GPG(gnupghome=destkeybox_path.as_posix())

            input_data = srckeybox.gen_key_input(key_type="RSA", key_length=1024, name_email="test@test", passphrase="vomi")
            srckeybox.gen_key(input_data)
            print(len(srckeybox.list_keys()))
            self.assertTrue(len(srckeybox.list_keys()) == 1)
            keyid = srckeybox.list_keys()[0]["keyid"]
            ascii_key = srckeybox.export_keys(keyid)

            ascii_key0 = gnupg_utils.choose_pubkey(srckeybox_path)

            self.assertTrue(ascii_key == ascii_key0)


if __name__ == '__main__':
    unittest.main()